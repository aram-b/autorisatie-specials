import React, { useState, useEffect } from "react"
import token from './token/token'
import jwt from 'jsonwebtoken'
import styled from 'styled-components'

/*

Conclusie: token verifieren in de frontend heeft niet zo veel zin volgens Michiel, dus token wordt alleen gebruikt om niet-gevoelige content te tonen voor ingelogde gebruikers en om door te sturen naar api.data.amsterdam. Daar gebeurt de daadwerkelijke autorisatie om bij gevoelige data te kunnen komen. 

*/

const URL = 'https://api.data.amsterdam.nl/dataselectie/hr/?page=1&dataset=ves&shape=%5B%5D&buurtcombinatie_naam=Burgwallen-Oude%20Zijde'

const ContentBlock = styled.div`
  padding: 16px;
  margin-top: 24px;
  margin-bottom: 24px;
  background-color: papayawhip;
`

const App = () => {
  const [data, setData] = useState()
  const decoded = jwt.decode(token)

  useEffect(() => {
    fetch(URL, { 
        method: 'get', 
        headers: new Headers({
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/x-www-form-urlencoded'
        })
      })
      .then(response => response.json())
      .then(json => setData(json))
  }, [])

  return (
    <>
      <div>{`Gebruiker is ${decoded ? decoded.sub : 'niet ingelogd'}`}</div>
      <div>{`Gebruiker heeft de volgende scopes: ${decoded ? decoded.scopes : 'geen'}`}</div>
      <ContentBlock>Deze content is publiek toegankelijk.</ContentBlock>
      {decoded && decoded.scopes.includes('BRK/RSN') ?
      <ContentBlock>
        <div>Deze content is alleen voor ingelogde gebruikers met scope BRK/RSN te zien.</div>
        <div>Dit nummer komt uit een API met autorisatie: {data ? data.object_list[0].kvk_nummer : null}</div>
      </ContentBlock>
      : null}
    </>
  )
}

export default App;